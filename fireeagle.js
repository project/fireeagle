
Drupal.behaviors.fireeagle = function (context) {
  $("#fireeagle-callback").hide();
  
  $("#fireeagle-start").bind("click", function() {
    window.open($(this).attr("href"), 'fireeagle', "width=800,height=600,resizable=yes,scrollbars=yes,status=yes");
    $(this).hide();
    $("#fireeagle-callback").show();
    return false;
  });
}