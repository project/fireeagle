<?php

function fireeagle_auth_start() {
  global $user;
  module_load_include('php', 'fireeagle');

  $fe = new FireEagle(FIREEAGLE_KEY, FIREEAGLE_SECRET);
  $tok = $fe->getRequestToken();
  if (!isset($tok['oauth_token'])
      || !is_string($tok['oauth_token'])
      || !isset($tok['oauth_token_secret'])
      || !is_string($tok['oauth_token_secret'])) {
    watchdog('error', t('ERROR! FireEagle::getRequestToken() returned an invalid response.  Giving up.'));
    drupal_goto();
  }
  $_SESSION['fireeagle']['auth_state'] = "start";
  $_SESSION['fireeagle']['request_token'] = $token = $tok['oauth_token'];
  $_SESSION['fireeagle']['request_secret'] = $tok['oauth_token_secret'];
  drupal_set_header("Location: ".$fe->getAuthorizeURL($token));
}

function fireeagle_auth_callback() {
  global $user;
  module_load_include('php', 'fireeagle');

  if (empty($_SESSION['fireeagle']['auth_state']) || $_SESSION['fireeagle']['auth_state'] != "start") {
    drupal_set_message(t('Fire eagle authentication request out of sequence.'), 'error');
    return;
  }
  $fe = new FireEagle(FIREEAGLE_KEY, FIREEAGLE_SECRET, $_SESSION['fireeagle']['request_token'], $_SESSION['fireeagle']['request_secret']);
  $tok = $fe->getAccessToken();
  if (!isset($tok['oauth_token']) || !is_string($tok['oauth_token'])
      || !isset($tok['oauth_token_secret']) || !is_string($tok['oauth_token_secret'])) {
    watchdog('error', t('Bad token from FireEagle::getAccessToken().'));
    drupal_set_message(t('Fire eagle returned an invaled response.'), 'error');
    return;
  }
  
  unset($_SESSION['fireeagle']);
  db_query("DELETE FROM {fireeagle_user} WHERE uid=%d", $user->uid);
  db_query("INSERT INTO {fireeagle_user} (uid, oauth_token, oauth_token_secret) VALUES (%d, '%s', '%s')",
           $user->uid, $tok['oauth_token'], $tok['oauth_token_secret']);
  fireeagle_fetch_user_location($user);
  drupal_goto();
}